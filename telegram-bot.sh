#!/bin/bash

# Declare curl max time limit.
declare TIME="10"

# Declare a remote URL address variable to send a message to the bot.
declare URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"

# Declare the job execution variable as bytes emoji.
declare STATUS=$(if [ $CI_JOB_STATUS = "success" ]; then echo -e "\xE2\x9C\x85"; else echo -e $"\xE2\x9D\x8C"; fi)

# Create an associative array from a string stored in Gitlab CI variables.
declare -A USER_ID="($(echo $TELEGRAM_USERS_ID))"

# Get matching GitLab user ID and telegram username if available.
declare USERNAME=$(if [ -n "${USER_ID[$GITLAB_USER_ID]}" ]; then echo $GITLAB_USER_NAME "(${USER_ID[$GITLAB_USER_ID]})"; else echo $GITLAB_USER_NAME; fi)

# Send message to telegram bot API.
curl -s --max-time $TIME -d "chat_id=$TELEGRAM_CHAT_ID&disable_web_page_preview=1&text=New merge request: %0A%0AStatus: $STATUS%0AUsername: $USERNAME%0ATitle: $CI_MERGE_REQUEST_TITLE%0ABranch: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME%0AMerge request: $CI_PROJECT_URL/merge_requests/$CI_MERGE_REQUEST_IID" $URL > /dev/null