# GitLab CI/CD Telegram bot

Allows you to send notifications to telegram chat through a bot after completing the specified stages GitLab CI/CD.

## Create Telegram bot
1. Create a Telegram bot using [@BotFather](https://t.me/BotFather).
2. Get a token to work with the Telegram API.
3. Get chat ID because telegram bot does not allow you to send messages from the bot to any user.

## Add CI/CD variables
Go to **Project > Settings > CI/CD > Variables** and add the following variables:

| Key                   | Value                                                         | Type      |
| --------------------- | --------------------------------------------------------------| --------- |
| TELEGRAM_BOT_TOKEN    | XXXXXXXXXXX:XXXXXXXXXXXXXXXXXXXXX_XXXXXX-XXXXXXXX             | Variable  |
| TELEGRAM_CHAT_ID      | -XXXXXXXXXX                                                   | Variable  |
| TELEGRAM_USERS_ID     | [GITLAB_USER_ID]="tg_username" [GITLAB_USER_ID]="tg_username" | Variable  |

## Create bash script
Create a `telegram-bot.sh` script that will access the Telegram API and send messages to the specified chat:

```bash
#!/bin/bash

# Declare curl max time limit.
declare TIME="10"

# Declare a remote URL address variable to send a message to the bot. <br/>
declare URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"

# Declare the job execution variable as bytes emoji.
declare STATUS=$(if [ $CI_JOB_STATUS = "success" ]; then echo -e "\xE2\x9C\x85"; else echo -e $"\xE2\x9D\x8C"; fi)

# Create an associative array from a string stored in Gitlab CI variables.
declare -A USER_ID="($(echo $TELEGRAM_USER_ID))"

# Get matching GitLab user ID and telegram username if available.
declare USERNAME=$(if [ -n "${USER_ID[$GITLAB_USER_ID]}" ]; then echo $GITLAB_USER_NAME "(${USER_ID[$GITLAB_USER_ID]})"; else echo $GITLAB_USER_NAME; fi)

# Send message to telegram bot API.
curl -s --max-time $TIME -d "chat_id=$TELEGRAM_CHAT_ID&disable_web_page_preview=1&text=New merge request: %0A%0AStatus: $STATUS%0AUsername: $USERNAME%0ATitle: $CI_MERGE_REQUEST_TITLE%0ABranch: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME%0AMerge request: $CI_PROJECT_URL/merge_requests/$CI_MERGE_REQUEST_IID" $URL > /dev/null
```

You can extend the notification sending script to the conditions you need, for example, by checking the name of the stage being executed:

```bash
if [ $CI_JOB_NAME == "test job" ]; then
   curl -s --max-time...
fi
```

## Create GitLab configuration options 
Create `.gitlab-ci.yml` that will call the previously created `telegram-bot.sh` script after the step is completed to have access to the gitlab `CI_JOB_STATUS` variable:

```
stages:
  - test

test job:
  stage: test
  image: node:16.15.1
  only:
    - merge_request
  script:
    - echo "Starting test stage"
  after_script:
    - chmod +x telegram-bot.sh
    - telegram-bot.sh
```

## Preview

After executing the job, the Telegram bot will send the following message:

![image](telegram-image.png)